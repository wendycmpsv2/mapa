import { Component, ElementRef, ViewChild } from '@angular/core';
import { GoogleMap } from '@capacitor/google-maps';

// import { Geolocation, Geoposition } from '@ionic-native/geolocation/ngx';
import { Geolocation, Geoposition } from '@awesome-cordova-plugins/geolocation/ngx';


import { NavController } from '@ionic/angular';
import { environment } from '../../environments/environment';


@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss'],
})
export class Tab1Page {

  lat: number;
  lng: number;
  total: string;

  @ViewChild('map') mapRef: ElementRef<HTMLElement>;
  newMap: GoogleMap;
  center: any = {
    lat: 19.5158538,
    lng: -99.1345358,
  };
  markerId: string;

  constructor(public navCtrl: NavController, public geolocation:Geolocation) {
    this.getGeolocation();


  }

  getGeolocation(){
    this.geolocation.getCurrentPosition().then((geoposition: Geoposition)=>{
      this.addListeners
      this.lat = geoposition.coords.latitude;
      this.lng = geoposition.coords.longitude;

      let latlat = this.center.lat;
      let lnglng = this.center.lng;

      this.total = this.calculateDistance(this.lat, latlat, this.lng, lnglng) + "KM";
    });
    
  }


  calculateDistance(lat, latlat, lng, lnglng){
    let p = 0.017453292519943295;
    let c = Math.cos;
    let a = 0.5 - c((lat-latlat) * p) / 2 + c(latlat * p) *c((lat) * p) * (1 - c(((lng- lnglng) * p))) / 2;
    let dis = (12742 * Math.asin(Math.sqrt(a)));
    return dis.toFixed(2);
}


  ngOnInit(){}

  ngAfterViewInit(){
    this.createMap();

  }

  async createMap() {
    try{
      this.newMap = await GoogleMap.create({
        id: 'capacitor-google-maps',
        element: this.mapRef.nativeElement,
        apiKey: environment.google_maps_api_key,
        config: {
          center: this.center,
          zoom: 13,
        },
      });
  
      this.addMarker(this.center.lat, this.center.lng);
      this.addListeners();
    }catch(e){
      console.log(e);
    }
  }

  async addMarker(lat, lng) {
    // if(this.markerId) this.removeMarker();
    this.markerId = await this.newMap.addMarker({
      coordinate:{
        lat: lat,
        lng: lng,
      },
      // title: ,
      draggable: true
    });
  }

  async removeMarker(id?){
    await this.newMap.removeMarker(id ? id : this.markerId);
  }
  
  async addListeners(){
    await this.newMap.setOnMarkerClickListener((event) =>{
      console.log('setOnMarkerClickListener',event);
      this.removeMarker(event.markerId);
    });

    await this.newMap.setOnMapClickListener((event) =>{
      console.log('setOnMapClickListener',event);
      console.log(event.latitude);
      console.log(event.longitude);

      this.addMarker(event.latitude, event.longitude);

        this.geolocation.getCurrentPosition().then((geoposition: Geoposition)=>{
          this.addListeners
          this.lat = event.latitude;
          this.lng = event.longitude;
    
          let latlat = this.center.lat;
          let lnglng = this.center.lng;
    
          this.total = this.calculateDistance(this.lat, latlat, this.lng, lnglng) + "KM";
        });
    });

  //   await this.newMap.setOnMyLocationButtonClickListener((event) =>{
  //     console.log('setOnMyLocationButtonClickListener', event);
  //     // this.addMarker(event.latitude, event.longitude);
  //   });

  //   await this.newMap.setOnMyLocationClickListener((event) =>{
  //     console.log('setOnMyLocationClickListener',event);
  //     this.addMarker(event.latitude, event.longitude);
  //   });
  // }
  }
}
